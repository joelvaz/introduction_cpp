//
//  main.cpp
//  LeapYear
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "LeapYear.hpp"

using namespace std;

int main() {
    
    LeapYear *leapYear;
    leapYear = new LeapYear();
    
    int numberOfTestPass = 0;
    int testId = 0;
    int testFail = 0;
    
    testId++;
    //Unit test one, not divisible by 4
    if( leapYear->checkIfLeap(2015) == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test two, divisible by 2 but not by 4
    if( leapYear->checkIfLeap(1970) == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test three, divisible by 4 but not by 100
    if( leapYear->checkIfLeap(1996) == true ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test four, divisible by 100 and not by 400
    if( leapYear->checkIfLeap(2100) == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test four, divisible by 400
    if( leapYear->checkIfLeap(2000) == true ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test four, divisible by 200 not by 400
    if( leapYear->checkIfLeap(1800) == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    cout << "Program failed " << testFail << " tests," << " out of " << testId << " tests" << endl;
    
    /*
    while(true){
        leapYear->readYear();
        if( leapYear->checkIfLeap(1) ){
            cout << "Leap Year!" << endl;
        }
        else { cout << "Not Leap Year!" << endl; }
    }
    */
    
    delete leapYear;
    leapYear = NULL;
    return 0;
}
