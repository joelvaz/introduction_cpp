//
//  LeapYear.cpp
//  LeapYear
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "LeapYear.hpp"

using namespace std;

LeapYear::LeapYear(){}

LeapYear::LeapYear(int year){
    LeapYear::year = year;
}

LeapYear::~LeapYear(){}

void LeapYear::readYear(){
    string userInput = "";
    
    cout << "Please insert year: " << endl;
    cin >> userInput;
    
    if( validateYear(userInput) ){
        LeapYear::year = stoi(userInput);
    }
    else{ cout << "Invalid year inserted" << endl; }
}

bool LeapYear::validateYear(string userInput){
    
    for(int i = 0; i < userInput.length(); i++){
        if(isdigit(userInput[i]) == false){
            LeapYear::year = 0;
            return false;
        }
    }
    return true;
}

bool LeapYear::checkIfLeap(int year){
        
    if( LeapYear::year > 0){
        year = LeapYear::year;
    }
    if ( year%4 == 0 ){
        if( year%100 == 0 && year%400 != 0){
            return false;
        }
        return true;
    }
    return false;
}
