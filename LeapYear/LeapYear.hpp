//
//  LeapYear.hpp
//  LeapYear
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef LeapYear_hpp
#define LeapYear_hpp

#include <stdio.h>

using namespace std;

class LeapYear{
public:
    LeapYear();
    LeapYear(int year);
    ~LeapYear();
    void readYear();
    bool validateYear(string year);
    bool checkIfLeap(int year);
private:
    int year = 0;
};

#endif /* LeapYear_hpp */
