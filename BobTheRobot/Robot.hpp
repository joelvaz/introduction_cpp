//
//  BobTheRobot.hpp
//  BobTheRobot
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef BobTheRobot_hpp
#define BobTheRobot_hpp

#include <iostream>

using namespace std;

class Robot{
public:
    virtual void greeting();
};

class Bob: public Robot{
public:
    void readInput();
    void greeting() override;
    void responseNormal();
    void responseCapsQuestion();
    void responseCaps();
    void responseEmpty();
    void responseStandard();
private:
    string question;
};

#endif /* BobTheRobot_hpp */
