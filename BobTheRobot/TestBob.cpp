//
//  main.cpp
//  BobTheRobot
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "Robot.hpp"

int main() {
    Bob bob;
    
    bob.greeting();
    bob.readInput();
    
    return 0;
}
