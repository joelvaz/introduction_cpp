//
//  HelloWorld.cpp
//  Introduction_Cpp
//
//  Created by Joel Vaz on 08/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include "HelloWorld.hpp"

using namespace std;

namespace HelloWorld{
    string hello(){
        return "Hello, World!";
    }
}

int main(){

    string quote = "";
    quote = HelloWorld::hello();
    
    cout << quote << endl;
    
    return 0;
}
