//
//  HelloWorld.hpp
//  Introduction_Cpp
//
//  Created by Joel Vaz on 08/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef HelloWorld_hpp
#define HelloWorld_hpp

#include <iostream>

#pragma once
namespace HelloWorld
{
    std::string hello();
}

#endif /* HelloWorld_hpp */
