//
//  Hamming.cpp
//  HammingDNA
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "Hamming.hpp"

using namespace std;

Hamming::Hamming(){};

Hamming::Hamming(string dna1, string dna2){
    dnaSequence1 = dna1;
    dnaSequence2 = dna2;
}

Hamming::~Hamming(){}

void Hamming::readSequences(){
    string in1, in2;
    
    testModeOnly = false;
    cout << "Insert the first DNA sequence: " << endl;
    cin >> in1;
    cout << "Insert the second DNA sequence: " << endl;
    cin >> in2;
    
    if( validateInput(in1) ){
        dnaSequence1 = in1;
    } else {
        cout << "Invalid 1st input" << endl;
    }
    if( validateInput(in2) ){
        dnaSequence2 = in2;
    } else {
        cout << "Invalid 2nd input" << endl;
    }
}

bool Hamming::validateInput(string userIn){
    
    bool validFlag = false;
    
    for(auto letterTest : userIn)
    {
        for(auto letterAcceptable : accetableChar){
            if(letterTest == letterAcceptable)
            {
                validFlag = true;
                break;
            }
        }
        if(!validFlag){
            return false;
            
        }
        else { validFlag = false; }
    }
    return true;
}

int Hamming::checkHamming(string dna1, string dna2){

    if(validateInput(dna1) && validateInput(dna2)){
        if(!testModeOnly){
            dna1 = dnaSequence1;
            dna2 = dnaSequence2;
        }
        int hammingNumber = 0;
        double maxSize = 0, minSize = 0;
    
        if(dna1.size() >= dna2.size()){
            maxSize = dna1.size();
            hammingNumber = maxSize - dna2.size();
            minSize = dna2.size();
        }
        else {
            maxSize = dna2.size();
            hammingNumber = maxSize - dna1.size();
            minSize = dna1.size();
        }
        for(double itr=0; itr < minSize; itr++){
            if( ( dna1[itr] != dna2[itr] ) ){
                hammingNumber++;
            }
        }
        return hammingNumber;
    }
    return -1;
}
