//
//  main.cpp
//  HammingDNA
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "Hamming.hpp"

using namespace std;

int main() {
    
    Hamming *hamming;
    hamming = new Hamming();
    
    int numberOfTestPass = 0;
    int testId = 0;
    int testFail = 0;
    
    testId++;
    //Unit test one, normal input
    if( hamming->checkHamming("AATTGG", "AATTGG") == 0 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
     
    testId++;
    //Unit test two, invalid second input
    if( hamming->checkHamming("AATTGG", "ABTTGG") == -1 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }

    testId++;
    //Unit test three, invalid first input
    if( hamming->checkHamming("ABTTGG", "AATTGG") == -1 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }

    testId++;
    //Unit test four, long chain valid inputs same lenght
    if( hamming->checkHamming("AATTGGAATCAATTGGAATTGGCCCCTT", "AATTGGAATCAATTGGAATTGGCCCCTT") == 0 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }

    testId++;
    //Unit test five, sequence 1 greater than sequence 2
    if( hamming->checkHamming("AATTGG", "AATTG") == 1 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test six, sequence 2 greater than sequence 1
    if( hamming->checkHamming("AATTG", "AATTGG") == 1 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test seven, sequence 1 with 5 differences
    if( hamming->checkHamming("AATTGGCCTTGG", "CCTTCGTCTAGG") == 5 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test eight, sequence 1 greater and with 3 differences
    if( hamming->checkHamming("AATTGGCC", "TTCTGGC") == 4 ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }

    cout << "Program failed " << testFail << " tests," << " out of " << testId << " tests" << endl;

    delete hamming;
    hamming = NULL;
    
    return 0;
}
