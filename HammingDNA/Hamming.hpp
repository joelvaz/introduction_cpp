//
//  Hamming.hpp
//  HammingDNA
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef Hamming_hpp
#define Hamming_hpp

#include <iostream>
using namespace std;

class Hamming{
public:
    Hamming();
    Hamming(string dna1, string dna2);
    ~Hamming();
    void readSequences();
    bool validateInput(string userIn);
    int checkHamming(string dna1, string dna2);
private:
    bool testModeOnly = true; 
    string dnaSequence1;
    string dnaSequence2;
    string accetableChar = {'A', 'C', 'T', 'G'};
};

#endif /* Hamming_hpp */
