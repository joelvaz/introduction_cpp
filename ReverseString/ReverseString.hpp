//
//  ReverseString.hpp
//  ReverseString
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef ReverseString_hpp
#define ReverseString_hpp

using namespace std;

class ReverseString{
    
public:
    ReverseString();
    ReverseString(string toReverse);
    ~ReverseString();
    void readString();
    string reverse(string toReverse);
private:
    string toReverse;
};

#endif /* ReverseString_hpp */
