//
//  main.cpp
//  ReverseString
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "ReverseString.hpp"

using namespace std;

int main() {
    ReverseString *reverseString;
    reverseString = new ReverseString();
    
    int numberOfTestPass = 0;
    int testId = 0;
    int testFail = 0;
    
    testId++;
    //Unit test one, normal string no caps
    if( reverseString->reverse("hello") == "olleh" ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test two, normal string with caps
    if( reverseString->reverse("Hello") == "olleH" ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test three, string with special characters
    if( reverseString->reverse("Hello-World!") == "!dlroW-olleH" ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    testId++;
    //Unit test four, string with spaces
    if( reverseString->reverse("Hello, World!") == "!dlroW ,olleH" ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    
    cout << "Program failed " << testFail << " tests," << " out of " << testId << " tests" << endl;
    
    delete reverseString;
    reverseString = NULL;
    
    return 0;
}
