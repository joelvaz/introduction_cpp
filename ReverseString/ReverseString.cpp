//
//  ReverseString.cpp
//  ReverseString
//
//  Created by Joel Vaz on 10/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "ReverseString.hpp"

using namespace std;

ReverseString::ReverseString(){}

ReverseString::ReverseString(string toReverse){
    ReverseString::toReverse = toReverse;
}

ReverseString::~ReverseString(){}

void ReverseString::readString(){
    cout << "Insert String to invert: " << endl;
    getline(cin, toReverse);
}

string ReverseString::reverse(string toReverse){
    string reversed;
    double lenghtString;
    
    if(ReverseString::toReverse.length() > 0){
        toReverse = ReverseString::toReverse;
    }
    
    lenghtString = toReverse.length();
    
    for(int i=0; i < lenghtString; i++){
        reversed.push_back(toReverse[lenghtString - i - 1]);
    }
    return reversed;
}
