//
//  Isogram.cpp
//  ExerciseIsogram
//
//  Created by Joel Vaz on 08/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "Isogram.hpp"


IsogramFinder::IsogramFinder(){};

IsogramFinder::IsogramFinder(string word){
    IsogramFinder::word = word;
}

IsogramFinder::~IsogramFinder(){};

void IsogramFinder::readWord(){
    std::cout << "Please insert the word for testing" << std::endl;
    if(validateInput())
    {
        getline(std::cin, IsogramFinder::word);
    }
}

bool IsogramFinder::validateInput(){
    
    if( !cin )
    {
        std::cout << "Input value is not valid!" << std::endl;
        return false;
    }
    else{
        return true;
    }
}

bool IsogramFinder::checkIsogram(){
    string newWord = word;
    int letterPosition = 0;
    bool IsogramFlag = true;
    
    for(char letterSeed : word){
        if( (letterSeed >= 'a' && letterSeed <= 'z') || (letterSeed >= 'A' && letterSeed <= 'Z')  ){
            newWord.erase(letterPosition, 1);
            for(char letterCompare : newWord){
                if(letterSeed == letterCompare || letterSeed == toupper(letterCompare) ){
                    IsogramFlag = false;
                    break;
                }
            }
            if(!IsogramFlag){
                //cout << word << " is not an Isogram!" << endl;
                return IsogramFlag;
            }
            newWord = word;
            letterPosition++;
        }
        else
            letterPosition++;
    }
    
    if(IsogramFlag){
        //cout << word << " is an Isogram!" << endl;
        return IsogramFlag;
    }
    return 0; 
}
