//
//  TestIsogram.cpp
//  ExerciseIsogram
//
//  Created by Joel Vaz on 09/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#include <iostream>
#include "Isogram.hpp"

using namespace std;

int main(){
    
    IsogramFinder *isogram;

    int numberOfTestPass = 0;
    int testId = 0;
    int testFail = 0;
    
    testId++;
    //Unit test one, empty word
    isogram = new IsogramFinder(" ");
    if( isogram->checkIsogram() == true ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;
    
    testId++;
    //Unit test two, "hello"
    isogram = new IsogramFinder("hello");
    if( isogram->checkIsogram() == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;

    testId++;
    //Unit test three, upper case heLlo
    isogram = new IsogramFinder("heLlo");
    if( isogram->checkIsogram() == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;

    testId++;
    //Unit test four, space between words
    isogram = new IsogramFinder("hello world");
    if( isogram->checkIsogram() == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;

    testId++;
    //Unit test five, special characters
    isogram = new IsogramFinder("Yest-erday");
    if( isogram->checkIsogram() == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;
    
    testId++;
    //Unit test six, special characters isogram
    isogram = new IsogramFinder("six-year-old");
    if( isogram->checkIsogram() == true ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;
    
    testId++;
    //Unit test seven, repeated first and last letter
    isogram = new IsogramFinder("angola");
    if( isogram->checkIsogram() == false ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;
    
    testId++;
    //Unit test eight, special characters isogram
    isogram = new IsogramFinder("six_year&old");
    if( isogram->checkIsogram() == true ){
        numberOfTestPass++;
    }
    else{ cout << "Test Failed -> " << testId << endl; testFail++; }
    delete isogram;
    
    cout << "Program failed " << testFail << " tests," << " out of " << testId << " tests" << endl;
    isogram = NULL;

    return 0;
}
