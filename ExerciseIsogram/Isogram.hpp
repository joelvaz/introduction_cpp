//
//  Isogram.hpp
//  ExerciseIsogram
//
//  Created by Joel Vaz on 08/08/2020.
//  Copyright © 2020 Joel Vaz. All rights reserved.
//

#ifndef Isogram_hpp
#define Isogram_hpp

#include <iostream>
using namespace std;

class IsogramFinder{
    
public:
    IsogramFinder();
    IsogramFinder(string word);
    ~IsogramFinder();
    
    void readWord();
    bool validateInput();
    bool checkIsogram();

private:
    string word;
};

#endif /* Isogram_hpp */
